/*
** Image.hpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 12:37:23 2015 Alexis Bertholom
// Last update Thu Apr 20 11:41:42 2017 Lucas
*/

#ifndef IMAGE_HPP_
# define IMAGE_HPP_

# include "types.hpp"

struct			SDL_Surface;

class			Image
{
public:
  enum			Type
    {
      Wrap,
      Copy
    };

public:
  Image(Uint w, Uint h);
  Image(SDL_Surface* img, Image::Type type = Image::Copy);
  ~Image();

public:
  void			putPixel(Uint x, Uint y, Color color);
  void			putRect(Uint x, Uint y, Uint w, Uint h, Color color);
  void			blit(Uint x, Uint y, Image& img, Uint w, Uint h);
  void			clear();
  void			fill(Color color);
  void			putGrid(int **tab, int x, int y);
  SDL_Surface*		getSurface();

public:
  static const Uint	max = ~0;

private:
  SDL_Surface*		_img;
  bool			_allocd;
  Uint			_size;
};

#endif
