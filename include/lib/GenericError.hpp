//
// GenericError.hpp for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
// 
// Made by bertho_d
// Login   <bertho_d@epitech.net>
// 
// Started on  Sun Aug  3 00:42:03 2014 bertho_d
// Last update Tue Aug  5 21:51:02 2014 bertho_d
//

#ifndef GENERICERROR_HPP_
# define GENERICERROR_HPP_

# include "Error.hpp"

enum			t_genericErrCode
{
  INVALID_WINSIZE = 0,
  WRONG_IMAGE_FORMAT
};

class			GenericError : public Error
{
public:
  GenericError(t_genericErrCode errcode);

protected:
  t_genericErrCode	_errcode;

private:
  static const char	*genericErrorMessages[];
};

#endif
