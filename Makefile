##
## Makefile for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
## 
## Made by bertho_d
## Login   <bertho_d@epitech.net>
## 
## Started on  Fri Jul 25 03:24:57 2014 bertho_d
## Last update Thu Apr 13 00:14:29 2017 Lucas
##

NAME		= jeu

SRCDIR		= src/
INCLDIR		= include/
LIBDIR		= lib/

SRC		= $(SRCDIR)main.cpp \
		  $(SRCDIR)$(LIBDIR)DevError.cpp \
		  $(SRCDIR)$(LIBDIR)Error.cpp \
		  $(SRCDIR)$(LIBDIR)FileError.cpp \
		  $(SRCDIR)$(LIBDIR)GenericError.cpp \
		  $(SRCDIR)$(LIBDIR)Image.cpp \
		  $(SRCDIR)$(LIBDIR)Input.cpp \
		  $(SRCDIR)$(LIBDIR)SDLContext.cpp \
		  $(SRCDIR)$(LIBDIR)SDLDisplay.cpp \
		  $(SRCDIR)$(LIBDIR)SDLError.cpp \
		  $(SRCDIR)$(LIBDIR)RNG.cpp

OBJ		= $(SRC:.cpp=.o)

CFLAGS		+= -O3
CFLAGS		+= -pedantic
CFLAGS		+= -W
CFLAGS		+= -Wall
CFLAGS		+= -Wextra
CFLAGS		+= -I$(INCLDIR)
CFLAGS		+= -I$(INCLDIR)$(LIBDIR)

LIBS		+= -lSDL2
LIBS		+= -lSDL2main

CC		= g++
RM		= rm -f

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(CFLAGS) $(LIBS) -o $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.cpp.o:
	$(CC) -c -o $@ $< $(CFLAGS)
