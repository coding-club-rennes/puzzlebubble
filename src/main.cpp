/*
** main.cpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 14:13:06 2015 Alexis Bertholom
// Last update Thu Apr 13 00:11:38 2017 Lucas
*/

#include <unistd.h>
#include "SDLDisplay.hpp"
#include "Input.hpp"
#include "Colors.hpp"
#include "RNG.hpp"
bool RNG::_initialized = false;

int		main()
{
  SDLDisplay	display("minijeu", 800, 800);
  Input		input;

  display.clearScreen();
  display.putRect(300, 200, 100, 50, Colors::Violet);
  display.refreshScreen();
  while (!(input.shouldExit()) && !(input.getKeyState(SDL_SCANCODE_ESCAPE)))
    {
      display.refreshScreen();
      input.flushEvents();
    }
  return (0);
}
