//
// SDLContext.cpp for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
// 
// Made by bertho_d
// Login   <bertho_d@epitech.net>
// 
// Started on  Sun Aug  3 22:38:29 2014 bertho_d
// Last update Sun Aug  3 23:42:53 2014 bertho_d
//

#include <SDL2/SDL.h>
#include "SDLError.hpp"
#include "SDLContext.hpp"

SDLContext::SDLContext(Uint32 flags)
{
  if (SDL_Init(flags) != 0)
    throw (SDLError("SDL initialization failed"));
}

SDLContext::~SDLContext()
{
  SDL_Quit();
}

bool		SDLContext::isInitialized() const
{
  return (true);
}
