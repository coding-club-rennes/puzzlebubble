//
// Error.cpp for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
// 
// Made by bertho_d
// Login   <bertho_d@epitech.net>
// 
// Started on  Sat Aug  2 02:43:41 2014 bertho_d
// Last update Mon Aug  4 01:19:52 2014 bertho_d
//

#include "Error.hpp"

Error::Error()
{
}

Error::Error(const char *message) : _message(message)
{
}

std::string const	&Error::what() const
{
  return (this->_message);
}

void		Error::addErrorSuffix(std::string suffix)
{
  if (suffix.size() > 0)
    {
      this->_message += ": ";
      this->_message += suffix;
    }
}
